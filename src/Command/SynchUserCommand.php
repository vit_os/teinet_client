<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 08.08.19
 * Time: 13:14
 */


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;
use App\Entity\User;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Psr\Container\ContainerInterface;


class SynchUserCommand extends Command
{

    private $salt        = 'salt_is_salt';
    private $hash;
    private $itemsOnPage = 100;
    private $container;
    private $userObject;
    private $date;
    private $content;
    private $em;
    private $httpClient;
    private $serverHost  = 'trinet_server.lh';


    protected static $defaultName = 'app:update-user';

    private $connection;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //создаем hash для отправки на сервер, каждый день hash меняется можно заморочится написать более сложный алгоритм
        $this->hash       = hash('md5', 'user' . date('Y-m-d') . $this->salt);
        $this->httpClient = HttpClient::create();// Создаем http клиента
        $this->userObject = $this->container->get('doctrine')->getRepository(User::class);

        $date   = $this->userObject->findBy([], ['update_dt' => 'desc'], 1);//выбираем самую свежую дату
        $format = 'Y-m-d_G:i:s';

        if (is_array($date) && !empty($date)) {
            $this->date = array_shift($date)->getUpdateDt()->format($format);//преобразуем дату в нужный нам формат
        }
        else {
            $this->date = date($format, '0000');// если таблица пустая то дата  1 января 1970
        }
        //запрос на авторизацию
        $response = $this->httpClient->request('POST', 'http://' . $this->serverHost . '/login', ['body' => ['key' => $this->hash],
        ]);
        $header   = $response->getHeaders();

        if (isset($header['set-cookie'])) {

            $array            = explode(';', array_shift($header['set-cookie']));
            $sessionId        = array_shift($array);
            $this->httpClient = HttpClient::create(['headers' => ['Cookie' => $sessionId]]);

        }

        // забираем контент
        $this->content = json_decode($response->getContent(), true);


        unset($response);
        $page = 0;
        //проверяем есть ли url перехода
        if (isset($this->content['url'])) {
            $data = $this->getData($page);
            if (isset($data['number_pages'])) {
                $numberPages = $data['number_pages'];
                for ($page = 0; $page < $numberPages; $page++) {
                    $output->writeln($page);
                    $this->em = $this->container->get('doctrine')->getManager();
                    $data     = $this->getData($page);

                    if (isset($data['data'])) {
                        $this->setData($data['data']);
                    }
                }
            }
        }
    }

    /**
     * @param array $data
     */

    private function setData(array $data)
    {
        foreach ($data as $user) {
            $obj = $this->getUserObject($user['id']);
            $obj->setLogin($user['login']);
            $obj->setEmail($user['email']);
            $obj->setUpdateDt(new \DateTime(date('Y-m-d H:i:s', $user['update_dt'])));
            $obj->setCreateDt(new \DateTime(date('Y-m-d H:i:s', $user['create_dt'])));
            $this->em->persist($obj);
            $this->em->flush($obj);
        }
    }

    /**
     * @param int $page
     * @return mixed
     */

    private function getData(int $page)
    {
        $response = $this->httpClient->request('POST', 'http://' . $this->serverHost . $this->content['url'] . '?items=' . $this->itemsOnPage . '&date=' . $this->date . '&page=' . $page, ['body' => ['key' => $this->hash],]);
        $data     = json_decode($response->getContent(), true);
        if (isset($data['url'])) {
            $this->content['url'] = $data['url'];
        }

        return $data;
    }

    /**
     * @param int $userId
     * @return User
     */

    private function getUserObject(int $userId)
    : User
    {
        $obj = $this->userObject->find($userId);
        if (empty($obj)) {
            $obj = new User();
        }

        return $obj;
    }
}